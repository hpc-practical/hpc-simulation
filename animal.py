class Animal():
    
    def __init__(self, x, y, sWorld):
        self.sWorld = sWorld
        self.setPosition(x,y)

    def setPosition(self, x, y):
        self.x = x
        self.y = y
        
    def getPosition(self):
        return [self.x, self.y]
    
    def checkDeath(self) -> bool:
        if self.energy <= 0:
            return True
        else: return False