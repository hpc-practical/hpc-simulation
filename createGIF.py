from PIL import Image
import glob
import config

frames = []
imgs = []
for i in range(config.SIMUALTION_STEPS+1):
    imgs.append(glob.glob("{num}.png".format(num=str(i))))
for im in imgs:
    newFrame = Image.open(im[0])
    frames.append(newFrame)

frames[0].save("simulation.gif", format="GIF", append_images=frames[1:], save_all=True, duration=300, loop=0)