
import numpy as np
import config
import pygame as pg

def visualize(world):
    world_copy=np.zeros((config.WORLD_SIZE*config.PIXEL_SIZE, config.WORLD_SIZE*config.PIXEL_SIZE))
    for x in range(world.shape[0]):
        for y in range(world.shape[1]):
            world_copy[x*config.PIXEL_SIZE:x*config.PIXEL_SIZE+config.PIXEL_SIZE,y*config.PIXEL_SIZE:y*config.PIXEL_SIZE+config.PIXEL_SIZE]=world[x][y]
    pg.init()
    screen = pg.display.set_mode((config.WORLD_SIZE*config.PIXEL_SIZE, config.WORLD_SIZE*config.PIXEL_SIZE))
    clock = pg.time.Clock()

    colors = np.array([[205,133,63], [0,255,0], [255, 255, 0], [255, 0, 0]])
    surface = pg.surfarray.make_surface(colors[world_copy.astype(int)])
    # surface = pg.transform.scale(surface, (500, 500))  # Scaled a bit.

    running = True
    while running:
        for event in pg.event.get():
            if event.type == pg.QUIT:
                running = False

        screen.fill("white")
        screen.blit(surface, (0, 0))
        pg.display.flip()
        clock.tick(60)