import matplotlib.pyplot as plt
import matplotlib.pylab as plab
import matplotlib as mpl
import numpy as np

def visualize(array, show=True, name="defaultName", save=False):
    newcmap = mpl.colors.ListedColormap(["peru", "green", "yellow", "red"])
    bounds = (0, 1, 2, 3, 4)
    norm = mpl.colors.BoundaryNorm(bounds, newcmap.N)
    f, ax = plt.subplots()
    ax.imshow(array, cmap=newcmap, norm=norm)
    if save:
        plt.savefig("{n}".format(n=name) + ".png")
    if show: plt.show()