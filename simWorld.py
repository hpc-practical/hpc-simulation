import numpy as np
import random
import config
from hunter import Hunter
from prey import Prey

class SimWorld:
    def __init__(self, size, percPopulation, percPlants, percPrey, percHunter) -> None:
        self.world = np.zeros((size, size))
        self.populateWorld(percPopulation, percPlants, percPrey, percHunter)

    def populateWorld(self, percPopulation, percPlants, percPrey, percHunter):
        """
        Populates a SimWorld with the given percentages which should be given in the range between 0 and 1. 
        The percentages of plants, prey and hunters should add up to 100.
        """
        
        for x in range(self.world.shape[0]):
            for y in range(self.world.shape[1]):
                self.world[x][y] = self.getIndType(percPopulation, percPlants, percPrey, percHunter)
                if self.world[x][y]==config.IDENT_PREY:
                    Prey.preys.append(Prey(x, y, self.world))
                elif self.world[x][y]==config.IDENT_HUNTER:
                    Hunter.hunters.append(Hunter(x, y, self.world))

    def getIndType(self, percPopulation, percPlants, percPrey, percHunter) -> int:
        """
        Randomly generates an individual type and returns the corresponding value.
        When percentages of plants, prey and hunters do not add up to 1, the hunter-population will be affected.
        """

        # ?? Can have a tile with plant+prey?
        number = random.random()
        plantLim = percPopulation * percPlants
        preyLim = percPopulation * percPrey + plantLim
        hunterLim = percPopulation * percHunter + preyLim
        if number <= plantLim:
            number = config.IDENT_PLANT
        elif number <= preyLim:
            number = config.IDENT_PREY
        elif number <= hunterLim:
            number = config.IDENT_HUNTER
        else: number = 0

        return number
    
    