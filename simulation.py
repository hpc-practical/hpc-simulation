import time
import numpy as np
import visualizer
import vpygame
import simWorld
import random
import config
import helper
from hunter import Hunter
from prey import Prey

class Simulation:
    def __init__(self, steps) -> None:
        self.steps = steps
        self.sWorld = simWorld.SimWorld(config.WORLD_SIZE, config.WORLD_POPULATION, config.WORLD_PLANTS_PERCENTAGE, config.WORLD_PREY_PERCENTAGE, config.WORLD_PREDATOR_PERCENTAGE)
        self.currentStep = 0
    
    def nextStep(self):
        """
        move hunter
        move prey
        spawn plants
        """
        self.currentStep += 1
        self.moveHunters()
        self.movePrey()
        self.spawnPlants(config.SPAWN_PLANTS_RATE)
    
    def spawnPlants(self, number):
        for i in range(number):
            x = random.randrange(config.WORLD_SIZE)
            y = random.randrange(config.WORLD_SIZE)
            if self.sWorld.world[x][y] == 0:
                self.sWorld.world[x][y] = config.IDENT_PLANT
            else:
                xy = helper.getClosestOfType(self.sWorld.world, x, y, 0)
                self.sWorld.world[xy["x"]][xy["y"]] = config.IDENT_PLANT

    def movePrey(self):
        for prey in Prey.preys:
            self.sWorld.world[prey.x][prey.y] = 0
            if not prey.checkDeath():
                prey.move()

    def moveHunters(self):
        for hunter in Hunter.hunters:
            self.sWorld.world[hunter.x][hunter.y] = 0
            if not hunter.checkDeath():
                hunter.move()
        




#TESTING

sim = Simulation(config.SIMUALTION_STEPS)
visualizer.visualize(sim.sWorld.world, True, sim.currentStep, True)
for i in range(config.SIMUALTION_STEPS):
    print("Preys:", len(Prey.preys))
    print("Hunters:", len(Hunter.hunters))
    sim.nextStep()
    visualizer.visualize(sim.sWorld.world, False, sim.currentStep, True)





