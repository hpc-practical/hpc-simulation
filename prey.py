import collections
from animal import Animal
import helper
import config

class Prey(Animal):
    preys=[]
    def __init__(self, x, y, sWorld) -> None:
        super().__init__(x,y,sWorld)
        self.energy = config.PREY_ENERGY

    #https://stackoverflow.com/questions/47896461/get-shortest-path-to-a-cell-in-a-2d-array-in-python
    def move(self):
        queue = collections.deque([[(self.x, self.y)]])
        seen = set([(self.y, self.y)])
        while queue:
            path = queue.popleft()
            x, y = path[-1]
            if self.sWorld[x][y] == config.IDENT_PLANT:
                self.setPosition(path[1][0], path[1][1])
                self.energy -= config.ENGERGY_DEPLETION_MOVE
                self.eat()
                return path
            for x2, y2 in ((x+1,y), (x-1,y), (x,y+1), (x,y-1), (x+1,y+1), (x-1,y-1), (x-1,y+1), (x+1,y-1)):
                if x2 < 0:
                    x2 = config.WORLD_SIZE + x2
                if x2 >= config.WORLD_SIZE:
                    x2 = x2 - config.WORLD_SIZE

                if y2 < 0:
                    y2 = config.WORLD_SIZE + y2
                if y2 >= config.WORLD_SIZE:
                    y2 = y2 - config.WORLD_SIZE

                if self.sWorld[y2][x2] != config.IDENT_PREY and self.sWorld[y2][x2] != config.IDENT_HUNTER and (x2, y2) not in seen:
                    queue.append(path + [(x2, y2)])
                    seen.add((x2, y2))

    def eat(self):
        if self.energy > config.FULL_ENERGY-config.PREY_ENERGY_BY_EATING:
            self.energy=config.FULL_ENERGY
        else: self.energy += config.PREY_ENERGY_BY_EATING
        self.sWorld[self.x][self.y] = config.IDENT_PREY

    @staticmethod
    def delete(x,y):
        for prey in Prey.preys:
            prey.__class__ = Prey
            if prey.getPosition()==[x,y]:
                Prey.preys.remove(prey)
