import config
import random 

def getClosestOfType(world, x, y, type):
    xy = {}
    for d in range(1, config.WORLD_SIZE-1):
        xmd = x-d
        if xmd < 0:
            xmd = config.WORLD_SIZE + xmd

        xpd = x+d
        if xpd > config.WORLD_SIZE-1:
            xpd -= config.WORLD_SIZE
            
        ymd = y-d
        if ymd < 0:
            ymd = config.WORLD_SIZE + ymd

        ypd = y+d
        if ypd > config.WORLD_SIZE-1:
            ypd -= config.WORLD_SIZE


        if world[xmd][y] == type:
            xy["x"] = xmd
            xy["y"] = y
            xy["direction"] = "top"
            xy["distance"] = d
            break
        elif world[xmd][ymd] == type:
            xy["x"] = xmd
            xy["y"] = ymd
            xy["direction"] = "topleft"
            xy["distance"] = d
            break
        elif world[x][ymd] == type:
            xy["x"] = x
            xy["y"] = ymd     
            xy["direction"] = "left"
            xy["distance"] = d
            break
        elif world[xpd][ymd] == type:
            xy["x"] = xpd
            xy["y"] = ymd
            xy["direction"] = "bottomleft"
            xy["distance"] = d
            break
        elif world[xpd][y] == type:
            xy["x"] = xpd
            xy["y"] = y
            xy["direction"] = "bottom"
            xy["distance"] = d
            break
        elif world[xpd][ypd] == type:
            xy["x"] = xpd
            xy["y"] = ypd
            xy["direction"] = "bottomright"
            xy["distance"] = d
            break
        elif world[x][ypd] == type:
            xy["x"] = x
            xy["y"] = ypd
            xy["direction"] = "right"
            xy["distance"] = d
            break
        elif world[xmd][ypd] == type:
            xy["x"] = xmd
            xy["y"] = ypd
            xy["direction"] = "topright"
            xy["distance"] = d
            break

    return xy

def saveWorldState():
    return None